"use strict";

const string = 'welcome a board';
console.log(string);

// создаем массив
const str = string.split(' ');

// меняем первую букву
const newstr = str.map(function (el) {
    return el[0].toUpperCase() + el.slice(1);
});

// преобразовываем в новую строку
const result = newstr.join(' ');
console.log(result);

// собираем все в ф-цию
function upperCaseAll(string) {
    let newstr = ''
    for (let el of string.split(' ')) {
            newstr = newstr + String(el[0].toUpperCase() + el.slice(1)) + " ";
    }
    return newstr;
}

const result1 = upperCaseAll('hello lectrum');
console.log(result1); // Hello Lectrum

const result2 = upperCaseAll('welcome a board'); 
console.log(result2); // 'Welcome A Board'