const gulp = require('gulp');
const { src, dest, series, parallel, watch } = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const scss = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const fileinclude = require('gulp-file-include');
const svgSprite = require('gulp-svg-sprite');
const replace = require('gulp-replace');
const fs = require( 'fs' ); 
const log = require( 'fancy-log' );

const app = './app';                      
const build = './dist';    


function browsersync() {
    browserSync.init({
        server : {
            baseDir: './app/'
            // baseDir: "./" + app + "/"
        },
        notify: false,
		port: 3000,
    });
}

function html() {
	return src('app/*.html') //
		// .pipe(plumber())
		.pipe(fileinclude()) //
		// .pipe(webphtml())
		.pipe(dest('dist/html')) //
		.pipe(browsersync.stream());//
}

function cleanFcn() {
    return gulp.src('dist', { read: false })
        .pipe(clean())
        .pipe(gulp.src('dist'));
}

function styles() {
    return src('app/styles/**/*.scss')
        .pipe(scss({ outputStyle: 'compressed' }))
        .pipe(concat('style.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 10 version'],
            grid: true
        }))
        .pipe(dest('dist/css'))
        .pipe(dest('app/styles/css'))
        .pipe(browserSync.stream())
}

function sprites() {
	return gulp.src( `${ app }/images/sprite_src/sprites/**/*.svg`)
		.pipe( svgSprite( {
			shape: {
				id: {
					separator: '-',
					generator: 'svg-%s' // Генерация класса для иконки svg-name-icon
				},
			},
			mode: {
				symbol: {
					dest: '',
					sprite: `./images/sprite_src/sprite.svg`, // Генерация файла svg
					inline: true,
					render: {
						scss: {
							template: './config/sprite/tmpl_scss.mustache', // Настройка стилей для спрайта
							dest: `./styles/scss/_sprites/` // Генерация файла стилей для спрайта
						}
					}
				}
			},
			variables: { // Базовая настройка
				baseFz: 20,
				prefixStatic: 'svg-'
			}
			} ) )
		.pipe( gulp.dest( `${ app }/` ) );
}

function svg_inline_build() {
	return gulp.src(`${ app }/*.html`)
		.pipe(replace(/<div id="svg_inline">(?:(?!<\/div>)[^])*<\/div>/g, () => {      // Поиск div с id svg_inline для того что бы вставить содержимое файла ./images/sprite_src/sprite.svg
			const svgInline = fs.readFileSync(`${app}/images/sprite_src/sprite.svg`, 'utf8');      // Открываем файл
			return '<div id="svg_inline">\n' + svgInline + '\n</div>';       // Вставляем в div с id svg_inline содержимое файла ./images/sprite_src/sprite.svg
		}))
		.on('error', err => {
			log.error(err.message);
		})
		.pipe(gulp.dest(`${ build }`));
}

function watching() {
    watch(['app/styles/scss/**/*.scss'], styles);
    watch(['app/*.html']).on('change', browserSync.reload);
}

exports.html = html;
exports.styles = styles;
exports.watching = watching;
exports.browsersync = browsersync;
exports.clean = cleanFcn;
exports.svgInlineBuild = svg_inline_build;
exports.sprite = sprites;

exports.default = parallel(styles, browsersync, watching);
exports.build = series(cleanFcn,
    parallel(sprites, styles, svg_inline_build ),
)



