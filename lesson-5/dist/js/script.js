"use strict";

/* Бургер-меню */

const burgerMenu = document.querySelector(".header__burger");
const menuHeader = document.querySelector(".header__menu");
if (burgerMenu) {
	burgerMenu.addEventListener("click", function (e) {
		document.body.classList.toggle("lock");
		burgerMenu.classList.toggle("active");
		menuHeader.classList.toggle("active");
	});
}

$(function () {
	/* Slider (Слайдер) */
	$("[data-slider]").slick({
		appendArrows: $(".event__intro"),
		nextArrow:
			'<div class="event__intro-btn header__content-btn"><a class="event__intro-btn--link"              >Следующий</a></div>',
		prevArrow: false,
	});
});
