const { src, dest } = require("gulp");
const gulp = require("gulp");
const browsersync = require("browser-sync").create();
const autoprefixer = require("gulp-autoprefixer");
const scss = require("gulp-sass");
const group_media = require("gulp-group-css-media-queries");
const plumber = require("gulp-plumber");
const del = require("del");
const imagemin = require("gulp-imagemin");
const uglify = require("gulp-uglify-es").default;
const rename = require("gulp-rename");
const fileinclude = require("gulp-file-include");
const clean_css = require("gulp-clean-css");
const newer = require("gulp-newer");
const svgSprite = require("gulp-svg-sprite");
const replace = require("gulp-replace");
const fs = require("fs");
const log = require("fancy-log");
const babelify = require("babelify");
const browserify = require("gulp-browserify");

const webp = require("gulp-webp");
// let webpcss = require("gulp-webp-css");
// let webphtml = require("gulp-webp-html");

let project_name = "dist";
let src_folder = "app";

let path = {
	build: {
		html: project_name + "/",
		js: project_name + "/js/",
		css: project_name + "/css/",
		images: project_name + "/images/",
	},
	src: {
		html: [src_folder + "/**/*.html", "!" + src_folder + "/_*.html"],
		js: src_folder + "/js/script.js",
		css: src_folder + "/styles/scss/style.scss",
		images: [
			src_folder + "/images/**/*.{jpg,png,svg,gif,ico,webp}",
			"!**/favicon.*",
		],
	},
	watch: {
		html: src_folder + "/**/*.html",
		js: src_folder + "/**/*.js",
		css: src_folder + "/styles/scss/**/*.scss",
		images: src_folder + "/images/**/*.{jpg,png,svg,gif,ico,webp}",
	},
	clean: "./" + project_name + "/",
};

function browserSync(done) {
	browsersync.init({
		server: {
			baseDir: "./" + project_name + "/",
		},
		notify: false,
		port: 3000,
	});
}
function html() {
	return src(path.src.html, {})
		.pipe(plumber())
		.pipe(fileinclude())
		.pipe(dest(path.build.html))
		.pipe(browsersync.stream());
}
function css() {
	return src(path.src.css, {})
		.pipe(plumber())
		.pipe(scss({ outputStyle: "expanded" }).on("error", scss.logError))
		.pipe(group_media())
		.pipe(
			autoprefixer({
				grid: true,
				overrideBrowserslist: ["last 5 versions"],
				cascade: true,
			})
		)
		.pipe(dest(path.build.css))
		.pipe(clean_css())
		.pipe(
			rename({
				extname: ".min.css",
			})
		)
		.pipe(dest(path.build.css))
		.pipe(browsersync.stream());
}

function js() {
	return src(path.src.js, {})
		.pipe(plumber())
		.pipe(fileinclude())
		.pipe(gulp.dest(path.build.js))
		.pipe(
			browserify({
				transform: [
					babelify.configure({ presets: ["@babel/preset-env"] }),
				],
			})
		)
		.pipe(
			uglify({
				toplevel: true,
			})
		)
		.on("error", function (err) {
			console.log(err.toString());
			this.emit("end");
		})
		.pipe(
			rename({
				suffix: ".min",
				extname: ".js",
			})
		)
		.pipe(dest(path.build.js))
		.pipe(browsersync.stream());
}

function images() {
	return src(path.src.images)
		.pipe(newer(path.build.images))
		.pipe(
			webp({
				quality: 75,
			})
		)
		.pipe(
			rename({
				extname: ".webp",
			})
		)
		.pipe(dest(path.build.images))
		.pipe(src(path.src.images))
		.pipe(newer(path.build.images))
		.pipe(
			imagemin({
				progressive: true,
				svgoPlugins: [{ removeViewBox: false }],
				interlaced: true,
				optimizationLevel: 3, // 0 to 7
			})
		)
		.pipe(dest(path.build.images));
}

function sprites() {
	return gulp
		.src(`${src_folder}/images/sprite_src/sprites/**/*.svg`)
		.pipe(
			svgSprite({
				shape: {
					id: {
						separator: "-",
						generator: "svg-%s", // Генерация класса для иконки svg-name-icon
					},
				},
				mode: {
					symbol: {
						dest: "",
						sprite: `./images/sprite_src/sprite.svg`, // Генерация файла svg
						inline: true,
						render: {
							scss: {
								template: "./config/sprite/tmpl_scss.mustache", // Настройка стилей для спрайта
								dest: `./styles/scss/_sprites/`, // Генерация файла стилей для спрайта
							},
						},
					},
				},
				variables: {
					// Базовая настройка
					baseFz: 20,
					prefixStatic: "svg-",
				},
			})
		)
		.pipe(gulp.dest(`${src_folder}/`));
}
function svg_inline_build() {
	return gulp
		.src([`${src_folder}/*.html`, `"!" + src_folder + "/_*.html"`])
		.pipe(
			replace(/<div id="svg_inline">(?:(?!<\/div>)[^])*<\/div>/g, () => {
				// Поиск div с id svg_inline для того что бы вставить содержимое файла ./images/sprite_src/sprite.svg
				const svgInline = fs.readFileSync(
					`${src_folder}/images/sprite_src/sprite.svg`,
					"utf8"
				); // Открываем файл
				return '<div id="svg_inline">\n' + svgInline + "\n</div>"; // Вставляем в div с id svg_inline содержимое файла ./images/sprite_src/sprite.svg
			})
		)
		.on("error", (err) => {
			log.error(err.message);
		})
		.pipe(gulp.dest(`${src_folder}`));
}

function infofile() {}
function cb() {}
function clean() {
	return del(path.clean);
}
function watchFiles() {
	gulp.watch([path.watch.images], images);
	gulp.watch([path.watch.html], html);
	gulp.watch([path.watch.css], css);
	gulp.watch([path.watch.js], js);
}

let build = gulp.series(
	clean,
	gulp.parallel(sprites, svg_inline_build, images, html, css, js)
);
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.html = html;
exports.css = css;
exports.js = js;
exports.infofile = infofile;
exports.images = images;
exports.svgInlineBuild = svg_inline_build;
exports.sprite = sprites;
exports.clean = clean;
exports.build = build;
exports.watch = watch;

exports.default = watch;

// Для запуска набрать: gulp
