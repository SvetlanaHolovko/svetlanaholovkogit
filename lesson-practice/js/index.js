"use strict";
function upperCaseFirst(string) {
    return string[0].toUpperCase() + string.slice(1);
}
// достаем все классы в HTMLCollection
const elems = document.getElementsByClassName('js-class');
console.log(elems);

// создаем массив
let elemsArray = Array.from(elems);

// меняем текст на странице
for (let params of elems) {
    params.innerHTML = upperCaseFirst(params.textContent);
}